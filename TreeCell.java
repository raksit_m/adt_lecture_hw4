
public class TreeCell<T extends Comparable<T>> {

	private T datum;

	private TreeCell<T> left, right;

	public TreeCell(T x) {

		datum = x;

	}

	public TreeCell(T x, TreeCell<T> lft, TreeCell<T> rgt) {

		datum = x;

		left = lft;

		right = rgt;

	}

	public T getDatum() {

		return datum;

	}

	public void setDatum(T newDatum) {

		datum = newDatum;

	}

	public TreeCell<T> getLeft() {

		return left;

	}

	public void setLeft(TreeCell<T> treeCell) {

		left = treeCell;

	}

	public TreeCell<T> getRight() {

		return right;

	}

	public void setRight(TreeCell<T> newRight) {

		right = newRight;

	}

	public boolean search(T x, TreeCell<T> node) {

		if(node == null) return false;

		if(node.getDatum().equals(x)) return true;

		if(node.getDatum().compareTo(x) > 0)

			return search(x, node.getLeft());

		else return search(x, node.getRight());

	}

	public static void preOrder(TreeCell<?> T) {

		if(T == null) return;

		System.out.print(T.getDatum() + " ");

		preOrder(T.getLeft());

		preOrder(T.getRight());
	}

	public static void inOrder(TreeCell<?> T) {

		if(T == null) return;

		inOrder(T.getLeft());

		System.out.print(T.getDatum() + " ");

		inOrder(T.getRight());
	}

	public void postOrder(TreeCell<?> T) {

		if(T == null) return;

		postOrder(T.getLeft());

		postOrder(T.getRight());

		System.out.print(T.getDatum() + " ");

	}

	// returns a reference to the root of binary search tree T with node x (the datum) removed

	public TreeCell<T> delete(T value, TreeCell<T> root) {

		if(!search(value, root)) return root;

		if(root.getLeft() == null && root.getRight() == null) {

			return null;
		}

		if(value.compareTo(root.getDatum()) < 0) {

			root.setLeft(delete(value, root.getLeft()));

		}

		else if(value.compareTo(root.getDatum()) > 0) {

			root.setRight(delete(value, root.getRight()));

		}

		else {

			if(root.getRight() != null) {

				TreeCell<T> temp = findMinimumRight(root.getRight());

				root.setDatum(temp.getDatum());

				root.setRight(delete(temp.getDatum(), root.getRight()));
			}

			else {

				TreeCell<T> temp = findMaximumLeft(root.getLeft());

				root.setDatum(temp.getDatum());

				root.setLeft(delete(temp.getDatum(), root.getLeft()));

			}
		}

		return root;
	}

	public TreeCell<T> findMinimumRight(TreeCell<T> treeCell) {

		if(treeCell.getLeft() == null) return treeCell;

		else return findMinimumRight(treeCell.getLeft());
	}

	public TreeCell<T> findMaximumLeft(TreeCell<T> treeCell) {

		if(treeCell.getRight() == null) return treeCell;

		else return findMaximumLeft(treeCell.getRight());
	}

	public TreeCell<T> removeDuplicates(TreeCell<T> treeCell) {

		if(treeCell.getLeft() == null) return null;

		treeCell.setLeft(removeDuplicates(treeCell.getLeft()));

		return treeCell;
	}

	public static void main(String[] args) {

		TreeCell<Integer> root = new TreeCell<Integer>(new Integer(5), null, null);

		TreeCell<Integer> subtree1 = new TreeCell<Integer>(new Integer(2), null, null);

		TreeCell<Integer> subtree2 = new TreeCell<Integer>(new Integer(12), null, null);

		TreeCell<Integer> subtree3 = new TreeCell<Integer>(new Integer(-4), null, null);

		TreeCell<Integer> subtree4 = new TreeCell<Integer>(new Integer(3), null, null);

		TreeCell<Integer> subtree5 = new TreeCell<Integer>(new Integer(9), null, null);

		TreeCell<Integer> subtree6 = new TreeCell<Integer>(new Integer(21), null, null);

		TreeCell<Integer> subtree7 = new TreeCell<Integer>(new Integer(19), null, null);

		TreeCell<Integer> subtree8 = new TreeCell<Integer>(new Integer(25), null, null);

		root.setLeft(subtree1);

		root.setRight(subtree2);

		subtree1.setLeft(subtree3);

		subtree1.setRight(subtree4);

		subtree2.setLeft(subtree5);

		subtree2.setRight(subtree6);

		subtree6.setLeft(subtree7);

		subtree6.setRight(subtree8);

		inOrder(root);

		System.out.println();

		root = root.delete(new Integer(12), root);

		inOrder(root);
	}
}
